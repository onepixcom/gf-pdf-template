<?php

/**
 * Template Name: Application form
 * Version: 0.1
 * Description: A basic "Application form" PDF template showing custom PDF templates in action
 * Author: Jake Jackson
 * Author URI: https://gravitypdf.com
 * Group: Sol System
 * License: GPLv2
 * Required PDF Version: 4.0
 * Tags: space, solar system, getting started
 */

/* Prevent direct access to the template (always good to include this) */
if ( ! class_exists( 'GFForms' ) ) {
	return;
}

?>
<style>
    @page {
        footer: pages_footer;
    }

    @page :first {
        footer: first_page_footer;
    }

    span {
        margin: 0;
        padding: 0;
    }

    table, p {
        margin-top: 0;
        margin-bottom: 15px;
    }

    h3 {
        color: rgb(182, 58, 49);
    }

    .header-image {
        width: 100%;
        margin-bottom: 200px;
    }

    .fields .field-headline {
        font-weight: bold;
    }

    .fields p {
        background-color: rgb(230, 230, 231);
    }

    .color-red {
        color: rgb(182, 58, 49);
    }

    .w-50 {
        width: 50%;
    }

    .hr-headline {
        background-color: rgb(182, 58, 49);
        color: #fff;
        font-weight: bold;
    }

    .mt-20 {
        margin-top: 20px;
    }

    .mb-10 {
        margin-bottom: 10px;
    }

    .m-0 {
        margin: 0;
    }

    table tr {
        background-color: rgb(230, 230, 231);
    }

    table tr th {
        font-weight: normal;
        text-align: left;
    }

    .weight-bold {
        font-weight: bold;
    }

    .weight-normal {
        font-weight: normal;
    }

    .no-bg p {
        background-color: #fff;
    }
</style>
<htmlpagefooter name="first_page_footer">
    <p>PAGE {PAGENO}</p>
</htmlpagefooter>
<htmlpagefooter name="pages_footer">
    <div style="display: table-cell;
    vertical-align: bottom;">
        <p style="float: left;">PAGE {PAGENO}</p>
        <img src="<?php echo __DIR__ ?>/images/logo.png"
             style="width: 100px; float: right;">
    </div>
</htmlpagefooter>
<div class="header-image">
    <img style="width: 300px; float: right;"
         src="<?php echo __DIR__ ?>/images/logo.png">
</div>
<h3>Application for Employment</h3>
<p>All applicants are considered for all positions without regard to race, religion, color, sex, gender, sexual orientation, pregnancy, age, national origin, ancestry, physical or mental disability, severe/morbid obesity, medical condition, military or veteran status, genetic information, marital status, ethnicity, alienage or any other protected classifica- tion, in accordance with applicable federal, state, and local laws. By completing this application, you are seeking to join a team of hardworking professionals dedicated to consistently delivering outstanding service to our customers and
    contributing to the financial success of the company, its clients, and its employees. Equal access to programs, services, and employment is available to all qualified persons. Those applicants requiring accommodation to complete the application and/or interview process should contact a management representative.</p>
<div class="fields w-50">
    <span class="field-headline">Position(s) Applied For:</span>
    <p><?= $form_data['field']['Position(s) Applied For:'] ?></p>
    <span class="field-headline">Location(s):</span>
    <p><?= $form_data['field']['Location(s):'] ?></p>
    <span class="field-headline">Loan Originator NMLS#</span>
    <p><?= $form_data['field']['Loan Originator NMLS#'] ?></p>
    <span class="field-headline">Date of Application</span>
    <p><?= $form_data['field']['Date of Application'] ?></p>
</div>
<div class="hr-headline mt-20 mb-10">CONTACT INFORMATION</div>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['Name'] ) ): ?>
        <span class="field-headline">Name</span>
        <table>
            <tr>
                <th><?= $form_data['field']['Name']['first'] ?></th>
                <th><?= $form_data['field']['Name']['middle'] ?></th>
                <th><?= $form_data['field']['Name']['last'] ?></th>
            </tr>
        </table>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['Street Address']['street'] )
	           || ! empty( $form_data['field']['Street Address']['street2'] )
	           || ! empty( $form_data['field']['Street Address']['city'] )
	           || ! empty( $form_data['field']['Street Address']['state'] )
	           || ! empty( $form_data['field']['Street Address']['zip'] ) ): ?>
        <span class="field-headline">Street Address</span>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['Street Address']['street'] ) ): ?>
                    <th><?= $form_data['field']['Street Address']['street'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['Street Address']['street2'] ) ): ?>
                    <th><?= $form_data['field']['Street Address']['street2'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['Street Address']['city'] ) ): ?>
                    <th><?= $form_data['field']['Street Address']['city'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['Street Address']['state'] ) ): ?>
                    <th><?= $form_data['field']['Street Address']['state'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['Street Address']['zip'] ) ): ?>
                    <th><?= $form_data['field']['Street Address']['zip'] ?></th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
    <div>
        <div style="float: left; width: 47%;">
			<?php if ( ! empty( $form_data['field']['Email Address'] ) ): ?>
                <span class="field-headline">Email Address:</span>
                <p><?= $form_data['field']['Email Address'] ?></p>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['11.Primary Telephone'] ) ): ?>
                <span class="field-headline">Primary Telephone:</span>
                <p><?= $form_data['field']['11.Primary Telephone'] ?></p>
			<?php endif; ?>
        </div>
        <div class="clear-fix"></div>
        <div style="float: right; width: 50%;">
			<?php if ( ! empty( $form_data['field']['10.Nickname'] ) ): ?>
                <span class="field-headline">Nickname:</span>
                <p><?= $form_data['field']['10.Nickname'] ?></p>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['12.Secondary Telephone'] ) ): ?>
                <span class="field-headline">Secondary Telephone:</span>
                <p><?= $form_data['field']['12.Secondary Telephone'] ?></p>
			<?php endif; ?>
        </div>
    </div>
</div>
<div class="hr-headline mt-20 mb-10">EMPLOYMENT EXPERIENCE</div>
<p>Please list the names of your present or previous employers in chronological order for the last 10 years with present or last employer listed first. Be sure to account for all peri- ods of time. If self-employed, give firm name and supply business references.</p>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['3.Position(s) Applied For:'] ) ): ?>
        <span class="field-headline color-red">Latest Employer</span>
        <p><?= $form_data['field']['3.Position(s) Applied For:'] ?></p>
	<?php endif; ?>
	
	<?php if ( ! empty( $form_data['field']['28.Address']['street'] )
	           || ! empty( $form_data['field']['28.Address']['street2'] )
	           || ! empty( $form_data['field']['28.Address']['city'] )
	           || ! empty( $form_data['field']['28.Address']['state'] )
	           || ! empty( $form_data['field']['28.Address']['zip'] ) ): ?>
        <span class="field-headline">Address</span>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['28.Address']['street'] ) ): ?>
                    <th><?= $form_data['field']['28.Address']['street'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['28.Address']['street2'] ) ): ?>
                    <th><?= $form_data['field']['28.Address']['street2'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['28.Address']['city'] ) ): ?>
                    <th><?= $form_data['field']['28.Address']['city'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['28.Address']['state'] ) ): ?>
                    <th><?= $form_data['field']['28.Address']['state'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['28.Address']['zip'] ) ): ?>
                    <th><?= $form_data['field']['28.Address']['zip'] ?></th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
    <span class="field-headline">Dates Employed</span>
    <table>
        <tr>
			<?php if ( ! empty( $form_data['field']['18.Dates Employed'] ) ): ?>
                <th>From:<?= $form_data['field']['18.Dates Employed'] ?></th>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['19.Dates Employed'] ) ): ?>
                <th>To:<?= $form_data['field']['19.Dates Employed'] ?></th>
			<?php endif; ?>
        </tr>
    </table>
	<?php if ( ! empty( $form_data['field']['20.Supervisor and Telephone Number'] ) ): ?>
        <span class="field-headline">Supervisor and Telephone Number</span>
        <p><?= $form_data['field']['20.Supervisor and Telephone Number'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['137.May we contact?'] ) ): ?>
        <span class="field-headline">May We Contact?</span>
        <p><?= $form_data['field']['137.May we contact?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['21.Pay Rate'] ) || empty( $form_data['field']['22.Pay Rate'] ) ): ?>
        <span class="field-headline">Pay Rate</span>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['21.Pay Rate'] ) ): ?>
                    <th>Starting:<?= $form_data['field']['21.Pay Rate'] ?>/hour</th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['22.Pay Rate'] ) ): ?>
                    <th>Final:<?= $form_data['field']['22.Pay Rate'] ?>/hour</th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['24.Job Title and Duties'] ) ): ?>
        <span class="field-headline">Job Title and Duties</span>
        <p><?= $form_data['field']['24.Job Title and Duties'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['25.Reason for Leaving'] ) ): ?>
        <span class="field-headline">Reason for Leaving</span>
        <p><?= $form_data['field']['25.Reason for Leaving'] ?></p>
	<?php endif; ?>
</div>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['27.Previous Employment (if applicable)'] ) ): ?>
        <span class="field-headline color-red">Previous Employer</span>
        <p><?= $form_data['field']['27.Previous Employment (if applicable)'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['37.Address']['street'] )
	           || ! empty( $form_data['field']['37.Address']['street2'] )
	           || ! empty( $form_data['field']['37.Address']['city'] )
	           || ! empty( $form_data['field']['37.Address']['state'] )
	           || ! empty( $form_data['field']['37.Address']['zip'] ) ): ?>
        <span class="field-headline">Address</span>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['37.Address']['street'] ) ): ?>
                    <th><?= $form_data['field']['37.Address']['street'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['37.Address']['street2'] ) ): ?>
                    <th><?= $form_data['field']['37.Address']['street2'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['37.Address']['city'] ) ): ?>
                    <th><?= $form_data['field']['37.Address']['city'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['37.Address']['state'] ) ): ?>
                    <th><?= $form_data['field']['37.Address']['state'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['37.Address']['zip'] ) ): ?>
                    <th><?= $form_data['field']['37.Address']['zip'] ?></th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['34.Dates Employed'] ) || ! empty( $form_data['field']['35.Dates Employed'] ) ): ?>
        <span class="field-headline">Dates Employed</span>
        <table>
            <tr>
                <th>From:<?= $form_data['field']['34.Dates Employed'] ?></th>
                <th>To:<?= $form_data['field']['35.Dates Employed'] ?></th>
            </tr>
        </table>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['36.Supervisor and Telephone Number'] ) ): ?>
        <span class="field-headline">Supervisor and Telephone Number</span>
        <p><?= $form_data['field']['36.Supervisor and Telephone Number'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['139.May we contact?'] ) ): ?>
        <span class="field-headline">May We Contact?</span>
        <p><?= $form_data['field']['139.May we contact?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['31.Pay Rate'] ) || ! empty( $form_data['field']['32.Pay Rate'] ) ): ?>
        <span class="field-headline">Pay Rate</span>
        <table>
            <tr>
                <th>Starting:<?= $form_data['field']['31.Pay Rate'] ?>/hour</th>
                <th>Final:<?= $form_data['field']['32.Pay Rate'] ?>/hour</th>
            </tr>
        </table>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['30.Job Title and Duties'] ) ): ?>
        <span class="field-headline">Job Title and Duties</span>
        <p><?= $form_data['field']['30.Job Title and Duties'] ?></p>
	<?php endif; ?>
    <br>
	<?php if ( ! empty( $form_data['field']['30.Job Title and Duties'] ) ): ?>
        <span class="field-headline">Reason for Leaving</span>
        <p><?= $form_data['field']['30.Job Title and Duties'] ?></p>
	<?php endif; ?>
</div>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['50.Have you ever been involuntarily terminated or asked to resign from any job?'] ) ): ?>
        <span class="field-headline">Have you ever been involuntarily terminated or asked to resign from any job?</span>
        <p><?= $form_data['field']['50.Have you ever been involuntarily terminated or asked to resign from any job?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['51.Do you have a non-compete agreement with any company that would prevent or limit you in performing any duties and responsibilities for this company?'] ) ): ?>
        <span class="field-headline">Do you have a non-compete agreement with any company that would prevent or limit you in performing any duties and responsibilities for this company?</span>
        <span>if yes, you will be required to upload a copy of you non-compete agreement</span>
        <p><?= $form_data['field']['51.Do you have a non-compete agreement with any company that would prevent or limit you in performing any duties and responsibilities for this company?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['52.Please explain any gaps in your employment history:'] ) ): ?>
        <span class="field-headline">Please explain any gaps in your employment history:</span>
        <p><?= $form_data['field']['52.Please explain any gaps in your employment history:'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['53.Please list any other experience, job related skills, additional languages, or other qualifications that you believe should be considered in evaluating your qualifications for employment.'] ) ): ?>
        <span class="field-headline">Please list any other experience, job related skills, additional languages, or other qualifications that you believe should be considered in evaluat- ing your qualifications for employment.</span>
        <p><?= $form_data['field']['53.Please list any other experience, job related skills, additional languages, or other qualifications that you believe should be considered in evaluating your qualifications for employment.'] ?></p>
	<?php endif; ?>
</div>
<pagebreak/>
<div class="hr-headline mt-20 mb-10">EDUCATION AND LICENSES</div>
<p>Please describe your educational background below.</p>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['57.What is your highest level of education completed?'] ) ): ?>
        <span class="field-headline">What is your highest level of education completed?</span>
        <p><?= $form_data['field']['57.What is your highest level of education completed?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['58.High School'] ) ): ?>
        <span class="field-headline color-red">High School</span>
        <p><?= $form_data['field']['58.High School'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['59.Address']['street'] )
	           || ! empty( $form_data['field']['59.Address']['street2'] )
	           || ! empty( $form_data['field']['59.Address']['city'] )
	           || ! empty( $form_data['field']['59.Address']['state'] )
	           || ! empty( $form_data['field']['59.Address']['zip'] ) ): ?>
        <span class="field-headline">Address</span>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['59.Address']['street'] ) ): ?>
                    <th><?= $form_data['field']['59.Address']['street'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['59.Address']['street2'] ) ): ?>
                    <th><?= $form_data['field']['59.Address']['street2'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['59.Address']['city'] ) ): ?>
                    <th><?= $form_data['field']['59.Address']['city'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['59.Address']['state'] ) ): ?>
                    <th><?= $form_data['field']['59.Address']['state'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['59.Address']['zip'] ) ): ?>
                    <th><?= $form_data['field']['59.Address']['zip'] ?></th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
    <div>
		<?php if ( ! empty( $form_data['field']['60.Diploma or GED received'] ) ): ?>
            <div style="float: left; width: 47%;">
                <span class="field-headline">Diploma or GED received</span>
                <p><?= $form_data['field']['60.Diploma or GED received'] ?></p>
            </div>
		<?php endif; ?>
		<?php if ( ! empty( $form_data['field']['131.Year Graduated'] ) ): ?>
            <div class="clear-fix"></div>
            <div style="float: right; width: 50%;">
                <span class="field-headline">Year Graduated</span>
                <p><?= $form_data['field']['131.Year Graduated'] ?></p>
            </div>
		<?php endif; ?>
    </div>
    <span class="field-headline color-red">Last University/College Attended</span>
	<?php if ( ! empty( $form_data['field']['62.Last University/College Attended'] ) ): ?>
        <p><?= $form_data['field']['62.Last University/College Attended'] ?></p>
	<?php endif; ?>
	
	
	<?php if ( ! empty( $form_data['field']['63.Address']['street'] )
	           || ! empty( $form_data['field']['63.Address']['street2'] )
	           || ! empty( $form_data['field']['63.Address']['city'] )
	           || ! empty( $form_data['field']['63.Address']['state'] )
	           || ! empty( $form_data['field']['63.Address']['zip'] ) ): ?>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['63.Address']['street'] ) ): ?>
                    <th><?= $form_data['field']['63.Address']['street'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['63.Address']['street2'] ) ): ?>
                    <th><?= $form_data['field']['63.Address']['street2'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['63.Address']['city'] ) ): ?>
                    <th><?= $form_data['field']['63.Address']['city'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['63.Address']['state'] ) ): ?>
                    <th><?= $form_data['field']['63.Address']['state'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['59.Address']['zip'] ) ): ?>
                    <th><?= $form_data['field']['63.Address']['zip'] ?></th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
    <div>
		<?php if ( ! empty( $form_data['field']['64.Degree Earned'] ) ): ?>
            <div style="float: left; width: 47%;">
                <span class="field-headline">Degree earned</span>
                <p><?= $form_data['field']['64.Degree Earned'] ?></p>
            </div>
		<?php endif; ?>
		<?php if ( ! empty( $form_data['field']['65.Field of Study'] ) ): ?>
            <div class="clear-fix"></div>
            <div style="float: right; width: 50%;">
                <span class="field-headline">Field of Study</span>
                <p><?= $form_data['field']['65.Field of Study'] ?></p>
            </div>
		<?php endif; ?>
    </div>
	<?php if ( ! empty( $form_data['field']['81.Describe Specialized Training, Experience, Skills and Extra- Curricular Activities'] ) ): ?>
        <span class="field-headline">Describe Specialized Training, Experience, Skills and Extra- Curricular Activities</span>
        <p><?= $form_data['field']['81.Describe Specialized Training, Experience, Skills and Extra- Curricular Activities'] ?></p>
	<?php endif; ?>
</div>
<div class="hr-headline mt-20 mb-10">BUSINESS/PROFESSIONAL REFERENCES</div>
<p>Please list three professional references we may contact (former managers and/or coworkers) of individuals who are not related to you.</p>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['68.Name and Title']['first'] ) || $form_data['field']['68.Name and Title']['last'] ): ?>
        <span class="field-headline color-red">1.</span>
        <div>
			<?php if ( ! empty( $form_data['field']['68.Name and Title']['first'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <span class="field-headline">Name</span>
                    <p><?= $form_data['field']['68.Name and Title']['first'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['68.Name and Title']['last'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <span class="field-headline">Title</span>
                    <p><?= $form_data['field']['68.Name and Title']['last'] ?></p>
                </div>
			<?php endif; ?>
        </div>
        <div>
			<?php if ( ! empty( $form_data['field']['71.Business Relationship'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <span class="field-headline">Business Relationship</span>
                    <p><?= $form_data['field']['71.Business Relationship'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['75.Number or Email Address'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <span class="field-headline">Number or Email Address</span>
                    <p><?= $form_data['field']['75.Number or Email Address'] ?></p>
                </div>
			<?php endif; ?>
        </div>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['77.Name and Title']['first'] ) || $form_data['field']['77.Name and Title']['last'] ): ?>
        <span class="field-headline color-red">2.</span>
        <div>
			<?php if ( ! empty( $form_data['field']['77.Name and Title']['first'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <span class="field-headline">Name</span>
                    <p><?= $form_data['field']['77.Name and Title']['first'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['77.Name and Title']['last'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <span class="field-headline">Title</span>
                    <p><?= $form_data['field']['77.Name and Title']['last'] ?></p>
                </div>
			<?php endif; ?>
        </div>
        <div>
			<?php if ( ! empty( $form_data['field']['73.Business Relationship'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <span class="field-headline">Business Relationship</span>
                    <p><?= $form_data['field']['73.Business Relationship'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['74.Number or Email Address'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <span class="field-headline">Number or Email Address</span>
                    <p><?= $form_data['field']['74.Number or Email Address'] ?></p>
                </div>
			<?php endif; ?>
        </div>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['78.Name and Title']['first'] ) || $form_data['field']['78.Name and Title']['last'] ): ?>
        <span class="field-headline color-red">3.</span>
        <div>
			<?php if ( ! empty( $form_data['field']['78.Name and Title']['first'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <span class="field-headline">Name</span>
                    <p><?= $form_data['field']['78.Name and Title']['first'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['78.Name and Title']['last'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <span class="field-headline">Title</span>
                    <p><?= $form_data['field']['78.Name and Title']['last'] ?></p>
                </div>
			<?php endif; ?>
        </div>
        <div>
			<?php if ( ! empty( $form_data['field']['72.Business Relationship'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <span class="field-headline">Business Relationship</span>
                    <p><?= $form_data['field']['72.Business Relationship'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['76.Number or Email Address'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <span class="field-headline">Number or Email Address</span>
                    <p><?= $form_data['field']['76.Number or Email Address'] ?></p>
                </div>
			<?php endif; ?>
        </div>
	<?php endif; ?>
</div>
<div class="hr-headline mt-20 mb-10">GENERAL INFORMATION</div>
<div class="fields">
	<?php if ( ! empty( $form_data['field']['84.Have you ever gone by another name?'] ) ): ?>
        <span class="field-headline">Have you ever gone by another name?</span>
        <p class="w-50"><?= $form_data['field']['84.Have you ever gone by another name?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['85.Is any additional information relative to name changes, use of an assumed name, or nickname necessary to enable a check on your work and educational record?'] ) ): ?>
        <span class="field-headline">Is any additional information relative to name changes, use of an assumed name, or nickname necessary to enable a check on your work and educational record?</span>
        <p class="w-50"><?= $form_data['field']['85.Is any additional information relative to name changes, use of an assumed name, or nickname necessary to enable a check on your work and educational record?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['86.Have you ever worked for this company before?'] ) ): ?>
        <span class="field-headline">Have you ever worked for this company before?(Required)</span>
        <p class="w-50"><?= $form_data['field']['86.Have you ever worked for this company before?'] ?></p>
	<?php endif; ?>
    <span class="field-headline">Were you referred or recruited to our company by a current employee?</span>
    <div>
		<?php if ( ! empty( $form_data['field']['87.Were you referred or recruited to our company by a current employee?'] ) ): ?>
            <div style="float: left; width: 47%;">
                <p><?= $form_data['field']['87.Were you referred or recruited to our company by a current employee?'] ?></p>
            </div>
		<?php endif; ?>
		<?php if ( ! empty( $form_data['field']['125.Name of Referer']['first'] || $form_data['field']['125.Name of Referer']['last'] ) ): ?>
            <div class="clear-fix"></div>
            <div style="float: right; width: 50%;">
                <p><?= $form_data['field']['125.Name of Referer']['first'] ?> <?= $form_data['field']['125.Name of Referer']['last'] ?></p>
            </div>
		<?php endif; ?>
    </div>
    <div>
		<?php if ( ! empty( $form_data['field']['88.On what date are you available to begin work?'] ) ): ?>
            <div style="float: left; width: 47%;">
                <span class="field-headline">On what date are you available to begin work?</span>
                <p><?= $form_data['field']['88.On what date are you available to begin work?'] ?></p>
            </div>
		<?php endif; ?>
		<?php if ( ! empty( $form_data['field']['89.Days/Hours available to work:'] ) ): ?>
            <div class="clear-fix"></div>
            <div style="float: right; width: 50%;">
                <span class="field-headline">Days/Hours available to work:</span>
                <p><?= $form_data['field']['89.Days/Hours available to work:'] ?></p>
            </div>
		<?php endif; ?>
    </div>
    <div>
		<?php if ( ! empty( $form_data['field']['90.Are you available to work:'] ) ): ?>
            <div style="float: left; width: 47%;">
                <span class="field-headline">Are you available to work</span>
                <p><?= $form_data['field']['90.Are you available to work:'] ?></p>
            </div>
		<?php endif; ?>
		<?php if ( ! empty( $form_data['field']['91.Minimum salary required:'] ) ): ?>
            <div class="clear-fix"></div>
            <div style="float: right; width: 50%;">
                <span class="field-headline">Minimum salary required:</span>
                <p><?= $form_data['field']['91.Minimum salary required:'] ?></p>
            </div>
		<?php endif; ?>
    </div>
	<?php if ( ! empty( $form_data['field']['93.If hired, would you have a reliable means of transportation to and from work?'] ) ): ?>
        <span class="field-headline">If hired, would you have a reliable means of transportation to and from work?</span>
        <p><?= $form_data['field']['93.If hired, would you have a reliable means of transportation to and from work?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['94.Can you travel if work requires it?'] ) ): ?>
        <span class="field-headline">Can you travel if work requires it?</span>
        <p><?= $form_data['field']['94.Can you travel if work requires it?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['96.Are you at least 18 years old?'] ) ): ?>
        <span class="field-headline">Are you at least 18 years old?</span>
        <span>Note: if under 18, hire is subject to verification that you are of minimum legal age.</span>
        <p><?= $form_data['field']['96.Are you at least 18 years old?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['97.If hired, can you present evidence of your identity and legal right to live and work in this country?'] ) ): ?>
        <span class="field-headline">If hired, can you present evidence of your identity and legal right to live and work in this country?</span>
        <p><?= $form_data['field']['97.If hired, can you present evidence of your identity and legal right to live and work in this country?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['98.Are you able to perform the essential job functions of the job for which you are applying with or without reasonable accommodation?'] ) ): ?>
        <span class="field-headline">Are you able to perform the essential job functions of the job for which you are applying with or without reasonable accommodation?</span>
        <span>Note: we comply with the ada and consider reasonable accommodation measures that may be necessary for qualified applicants/employees to perform essential job func- tions.</span>
        <p><?= $form_data['field']['98.Are you able to perform the essential job functions of the job for which you are applying with or without reasonable accommodation?'] ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['99.Do you have friends or family that work at our company?'] ) || ! empty( $form_data['field']['132.Please List Friend or Family Member'] ) ): ?>
        <span class="field-headline">Do you have friends or family that work at our company?</span>
        <div>
			<?php if ( ! empty( $form_data['field']['99.Do you have friends or family that work at our company?'] ) ): ?>
                <div style="float: left; width: 47%;">
                    <p><?= $form_data['field']['99.Do you have friends or family that work at our company?'] ?></p>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $form_data['field']['132.Please List Friend or Family Member'] ) ): ?>
                <div class="clear-fix"></div>
                <div style="float: right; width: 50%;">
                    <p><?= $form_data['field']['132.Please List Friend or Family Member'] ?></p>
                </div>
			<?php endif; ?>
        </div>
	<?php endif; ?>
</div>
<div class="hr-headline mt-20 mb-10">APPLICANT STATEMENT AND AGREEMENT</div>
<div class="fields no-bg">
    <span class="field-headline">Please read and initial each paragraph below. If there is anything that you do not understand, please ask.</span>
	<?php if ( ! empty( $form_data['field']['102'][0] ) ): ?>
        <p class="m-0">I hereby authorize the company to thoroughly investigate my references, work record, education and other matters related to my suitability for employment and, further, autho- rize the prior employers and references i have listed to disclose to the company any and all letters, reports and other information related to my work records, without giving me prior notice of such disclosure. In addition, i hereby release the company, my former employers and all other persons, corporations, partnerships and associations from any and all claims, demands or liabilities arising out
            of or in any way
            related to such investigation or disclosure.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['103'][0] ) ): ?>
        <p class="m-0">In the event of my employment with the company, i understand that i am required to comply with all rules and regulations of the company.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['104'][0] ) ): ?>
        <p class="m-0">If hired, i understand and agree that my employment with the company is at-will, and that neither i, nor the company is required to continue the employment relationship for any specific term. I further understand that the company or i may terminate the employment relationship at any time, with or without cause, and with or without notice. I understand that the at-will status of my employment cannot be amended, modified, or altered in any way by any oral modifications.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['105'][0] ) ): ?>
        <p class="m-0">I understand that safety of employees is extremely important to the company and that the company is committed to ensuring a safe working environment. I understand that i, and every employee, have a responsibility to prevent accidents and injuries by observing all safety procedures and guidelines and following the directions of my site supervi- sor. I understand and agree to comply with federal, state, and local regulations related to on-the-job safety and health.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['106'][0] ) ): ?>
        <p class="m-0">I hereby certify that the answers given by me are true and correct to the best of my knowledge. I further certify that i, the undersigned applicant, have personally completed this application. I understand that any omission or misstatement of material fact on this application or on any document used to secure employment shall be grounds for rejection of this application or for immediate discharge if i am employed, regardless of the time elapsed before discovery.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['107'][0] ) ): ?>
        <p class="m-0">I understand that if i am selected for hire, it will be necessary for me to provide satisfactory evidence of my identity and legal authority to work in the united states, and that federal immigration laws require me to complete an i-9 form in this regard.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['108'][0] ) ): ?>
        <p class="m-0">I understand that if any term, provision, or portion of this agreement is declared void or unenforceable, it shall be severed and the remainder of this agreement shall be enforce- able.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['126'][0] ) ): ?>
        <p class="m-0">I agree that except at the request and for the benefit of the company, i will not disclose to anyone or use for my own purposes any of the company’s confidential or proprietary information, either during or after my employment. I understand and agree that the company’s trade secrets, bidding, costs, pricing and marketing information and techniques, designs, methods of engineering and production, financial and market information, computer software, sources of supply, customer names and information and employee names and information are confidential and proprietary
            information
            of the
            company; i also agree that i will not make written or other copies of notes regarding these matters except as necessary to perform my job, and i agree that if my employment with the company ends, i will deliver to the company all material of any kind that i have relating to the company, including any such copies or notes. I also agree that i will disclose and assign to the company any invention, design or process which i conceive or develop while employed by the company relating to the company’s business or to any product or service offered or being developed by the company, and
            that
            all such designs or concep- tions shall be the property of the company.
        </p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['127'][0] ) ): ?>
        <p class="m-0">I agree that the company and subsidiaries may conduct reasonable inspections of any, desks, hardware, software or other company property i may be using, and of any of my own property i bring onto the company’s premises (including vehicles, packages and purses) at any time, and i waive and promise not to make any claims against the com- pany (or its employees, directors, owners or agents) relating to such inspection. I understand the company may monitor certain employee activity, particularly communications and the use of communication devices. Use of such communication
            devices as these
            may be monitored: telephones, fax machines, computers, e-mails, web pages, cell phones and pagers.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['128'][0] ) ): ?>
        <p class="m-0">
            I understand the company has/may adopt a binding arbitration agreement to resolve any disputes concerning complaints i have about my employment or terms of my employ- ment. I agree to abide by the company’s binding arbitration agreement and waive my right to trial to resolve these issues. I understand that having a job with the company is consideration for agreeing to this. In addition, the binding arbitration agreement will be for my benefit and the company’s since it will save us both time and money to resolve issues.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['129'][0] ) ): ?>
        <p class="m-0">I hereby give my consent to the company and its agents or independent contractors, to perform appropriate tests or examinations on me for alcohol, illegal drugs, and/or other pre-employment tests, including a pre-employment physical examination, with the results of these tests or examinations to be released to the company for whatever use it deems fair and appropriate under the circumstances.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
</div>
<div class="fields no-bg">
    <span class="field-headline">MY SIGNATURE BELOW ATTESTS TO THE FACT THAT I HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE ABOVE TERMS</span>
    <br>
    
    <br>
    <span class="weight-bold color-red">Electronic signature agreement</span>
    <br>
    <br>
	<?php if ( ! empty( $form_data['field']['114'][0] ) ): ?>
        <p class="m-0">You agree to use an electronic document and an electronic signature. You understand that electronic signatures are legally binding in the United States and other countries.</p>
        <span class="weight-bold">—DSE</span>
        <br>
        <br>
	<?php endif; ?>
	<?php if ( ! empty( $form_data['field']['112.Name']['first'] )
	           || ! empty( $form_data['field']['112.Name']['last'] )
	           || ! empty( $form_data['field']['110.Date'] )
	           || ! empty( $form_data['field']['111.City/State'] ) ): ?>
        <span class="field-headline">Name</span>
        <table>
            <tr>
				<?php if ( ! empty( $form_data['field']['112.Name']['first'] ) ): ?>
                    <th><?= $form_data['field']['112.Name']['first'] ?></th>
				<?php endif; ?>
				<?php if ( ! empty( $form_data['field']['112.Name']['last'] ) ): ?>
                    <th><?= $form_data['field']['112.Name']['last'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['110.Date'] ) ): ?>
                    <th><?= $form_data['field']['110.Date'] ?></th>
				<?php endif; ?>
            </tr>
            <tr>
				<?php if ( ! empty( $form_data['field']['111.City/State'] ) ): ?>
                    <th><?= $form_data['field']['111.City/State'] ?></th>
				<?php endif; ?>
            </tr>
        </table>
	<?php endif; ?>
</div>